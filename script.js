Pusher.logToConsole = true;

const pusher = new Pusher("fa7ee674664954c1f5be", {
  cluster: "eu",
});

function logOut() {
  localStorage.setItem("id", null);
  const AllMessages = document.getElementById("messages");
  AllMessages.innerHTML = "";
}
function getMD5(body) {
  return CryptoJS.MD5(JSON.stringify(body));
}
// Enable pusher logging - don't include this in production

function getAuthSignature(md5, timeStamp) {
  return CryptoJS.HmacSHA256(
    `POST\n/apps/1259459/events\nauth_key=fa7ee674664954c1f5be&auth_timestamp=${timeStamp}&auth_version=1.0&body_md5=${md5}`,
    "2d1293ec93e526dd6cf1"
  );
}

let sendMessage = async function () {
  const userName = document.getElementById("username").value;
  const message = document.getElementById("messageText").value;
  const senderId = localStorage.getItem("id");
  const data = {
    message: message,
    sender: userName,
    senderId: senderId,
    createdAt: Date.now(),
  };
  let body = {
    data: JSON.stringify(data),
    name: "my-event",
    channel: newChannel,
  };
  let timeStamp = Date.now() / 1000;
  let md5 = getMD5(body);
  let url = `https://cors.bridged.cc/https://api-eu.pusher.com/apps/1259459/events?body_md5=${md5}&auth_version=1.0&auth_key=fa7ee674664954c1f5be&auth_timestamp=${timeStamp}&auth_signature=${getAuthSignature(
    md5,
    timeStamp
  )}`;
  let req = await fetch(url, {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
    },
  });
};

function join(e) {
  e.preventDefault();
  const uniqueUserId = Math.random().toString();
  localStorage.setItem("id", uniqueUserId);
  const AllMessages = document.getElementById("messages");
  AllMessages.innerHTML = "";
  const userName = document.getElementById("username").value;
  newChannel = document.getElementById("channel").value;
  const channel = pusher.subscribe(newChannel);
  console.log(channel);
  channel.bind("my-event", function (data) {
    addMessage(data);
  });

  const link = document.getElementById("ChatWindow");
  link.dispatchEvent(clickEvent);
}

const messages = [];

function addMessage(message) {
  messages.push(message);

  console.log("message");
  const AllMessages = document.getElementById("messages");
  AllMessages.innerHTML = `<div></div>`;
  messages

    .sort((a, b) => a.createdAt - b.createdAt)
    .forEach((message, index) => {
      const messageHTML = `
<div>
        <div class="inline uk-padding uk-background-muted uk-width-1-2@s">
        ${
          localStorage.getItem("id") == message.senderId
            ? `<label class="uk-label uk-label-success">me: </label>`
            : `<label class="uk-label uk-label-primary">${message.sender}</label>`
        }
        <p id="${message.createdAt}">${message.message}</p>
        
                    
        </div>

</div>`;
      AllMessages.insertAdjacentHTML("beforeEnd", messageHTML);
    });
}

const clickEvent = new MouseEvent("click", {
  view: window,
  bubbles: true,
  cancelable: false,
});
